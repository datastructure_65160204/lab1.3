public class MergeSortedArray {
    public static void main(String[] args) {
        int[] nums1 = {1, 2, 3, 0, 0, 0};
        int[] nums2 = {2, 5, 6};
        int m = 3; // จำนวนสมาชิกใน nums1
        int n = 3; // จำนวนสมาชิกใน nums2

        merge(nums1, m, nums2, n);

        // แสดงผลลัพธ์ของอาร์เรย์ที่รวม
        for (int num : nums1) {
            System.out.print(num + " ");
        }
    }

    public static void merge(int[] nums1, int m, int[] nums2, int n) {
        int p1 = m - 1; // Pointer สำหรับ nums1
        int p2 = n - 1; // Pointer สำหรับ nums2
        int mergeIndex = m + n - 1; // Index สำหรับอาร์เรย์ที่รวม

        // ทำการรวมสมาชิกตั้งแต่ท้ายของทั้งสองอาร์เรย์
        while (p1 >= 0 && p2 >= 0) {
            if (nums1[p1] >= nums2[p2]) {
                nums1[mergeIndex] = nums1[p1];
                p1--;
            } else {
                nums1[mergeIndex] = nums2[p2];
                p2--;
            }
            mergeIndex--;
        }

        // คัดลอกสมาชิกที่เหลืออยู่จาก nums2 ไปยัง nums1
        while (p2 >= 0) {
            nums1[mergeIndex] = nums2[p2];
            p2--;
            mergeIndex--;
        }
    }
}